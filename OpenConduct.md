# OpenConduct


## Brief and Purpose

OpenConduct is not a political document. What it is, is a document designed to prevent collaborative projects from becoming exclusive and inaccessible. The points expressed within this document are designed to value experience and willingness to contribute to a project, while also setting clear guidelines in place for what is and isn't expected of individuals within a project.

Most importantly OpenConduct aims to:
* Make the project accessible to as many contributors as possible.
* Review all contributions equally.
* Enable people to freely share their ideas, and to receive _constructive_ freedback in a safe environment.
* Prevent belittling, insults, and other personal attacks directed toward individuals and groups.

## Conduct

It is often helpful to maintain an atmosphere of constructive criticism within a project in order to ensure that each contributor's views are expressed and considered. As such contributors should be prepared to have their contributions and the ideas behind them carefully reviewed by others. In saying this, contributors are expected to treat each other with dignity when discussing or reviewing changes to the project.