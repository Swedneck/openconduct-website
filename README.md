# OpenConduct


## Brief and Purpose

OpenConduct is not a political document. What it is, is a document designed to prevent collaborative projects from becoming exclusive and inaccessible. The points expressed within this document are designed to value experience and willingness to contribute to a project, while also setting clear guidelines in place for what is and isn't expected of individuals within a project.

Most importantly OpenConduct aims to:
* Make the project accessible to as many contributors as possible.
* Ensure all contributions are reviewed equally.
* Enable people to freely share their ideas and receive _constructive_ criticism.
* Prevent belittling, needless insults, and other personal attacks directed toward individuals and groups.

## Conduct

It is often helpful to maintain an atmosphere of constructive criticism within a project in order to ensure that each contributor's views are expressed and considered, as such contributors should be prepared to have their contributions and the ideas behind them carefully reviewed and scrutinised by others. In saying this, contributors are expected to treat each other with dignity and mutual respect when discussing or reviewing changes, and should ensure the feedback they provide is based solely on the work and ideas that were presented as opposed to the person who presented them. That is to say, criticism should *not* be used as a means to attack the character or affiliations of another contributor, and instead should be limited to the ideas and contributions that have been presented to the project. These simple guidelines serve many purposes, chiefly they allow contributors to collect vital feedback on their work without fear of being attacked or belittled for reasons not concerning the project they are contributing to.
